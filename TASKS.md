# Cauldron Tasks

This document contains a list of tasks (and their description) to be performed by the workers on [Cauldron](https://gitlab.com/cauldronio).

## Task Workflow Graph

```mermaid
stateDiagram
    [*] --> Ready
    Ready --> Running
    Running --> Ready
    Running --> Finish
```

## Task states

### Ready

This state implies that the task has been requested and is waiting to be picked up by a worker.

* **Conditions to enter (from Running)**:
  * If the task uses tokens and the registered tokens are in cooldown
* **Actions done when entering (from Running)**:
  * The task status changes to **Ready**
  * The **Scheduled date** field is set as the value of the smallest **Rate time** of the registered tokens
  * The value of the **Retries** field is increased by one
  * The value of the **Assigned worker** field is set to None
* **Actions done when entering (from Init)**:
  * The task status changes to **Ready**
  * The **Creation date** field is set to the current time
  * The user requesting the task is added to the user set of the task

### Running

This status implies the assignment of a worker to the task and the beginning of its execution.

* **Conditions to enter (from Ready)**:
  * Have valid tokens (if needed)
  * Have a value for the **Scheduled date** field lower than the current time (or that is null)
* **Actions done when entering (from Ready)**:
  * The task status changes to **Running**
  * The **Start date** field is set to the current time
  * The **Assigned worker** field is set to the id of the worker performing the task

### Finish

This status implies the completion of a task, either because it has finished successfully or with error.

* **Conditions to enter (from Running)**:
  * It depends on the type of task
* **Actions done when entering (from Running)**:
  * The task status changes to **Success** or **Error**
  * The **Finish date** field is set to the current time
  * Specific actions described in the **Actions at finish** section of each type of task (only on **Success**)

## Common parameters

All tasks have parameters in common:

* Status **[string / int]**:
  * Pending
  * Running
  * Success
  * Error
  * Scheduled
* Users who requested the task **[model array]**
* Assigned worker **[model / string / int]**
* Creation date **[datetime]**
* Start date **[datetime]**
* Scheduled date **[datetime]**
* Finish date **[datetime]**
* Retries **[int]**

## Tasks related with `sirmordred`:

### Gather raw data

This task involves collecting data from any backend, using `sirmordred`. It may be necessary to use a token, depending on the backend.

* **Parameters**:
  * Repository **[model]**
* **Creation conditions**:
  * A user asks to analyze a repository
  * When a repositories collection task finish with success
* **Actions at running**:
  * Use `sirmordred` to collect data from the selected repository and store this data in Elasticsearch
  * If there are no registered tokens (for non-git backends), the task ends in **Error**
* **Actions at finish**:
  * An enrichment task is created

### Enrich raw data

This task is about enriching the raw data of a repository collected from any backend, using `sirmordred`. No token needed.

* **Parameters**:
  * Repository **[model]**
* **Creation conditions**:
  * When a gathering raw data task finish with success
* **Actions at running**:
  * Use `sirmordred` to enrich the raw data of the selected repository and store it in Elasticsearch
* **Actions at finish**:
  * An identity merge task is created

### Merge identities

This task involves merging identities using the enriched data of a repository.

* **Parameters**:
  * Repository **[model]**
* **Creation conditions**:
  * When an enrichment raw data task finish with success
* **Actions at running**:
  * Use `sirmordred` to merge the identities present in the enriched data of the selected repository
* **Actions at finish**:
  * None

## Another tasks:

### Collect repositories list

This task is about collecting the list of repositories associated with a particular user (for certain backends), using external APIs

* **Parameters**:
  * Backend user **[string]**
  * Backend **[model / string]**
* **Creation conditions**:
  * A user asks to analyze every repository of a user (GitHub / Gitlab)
* **Actions at running**:
  * Use the API of the selected backend to collect the list of repositories of the selected user (More than one API call will be required)
  * If there are no registered tokens (for non-git backends), the task ends in **Error**
* **Actions at finish**:
  * The necessary Django models are created
  * A raw data collection task is created for each repository found

### Collect project info

This task is used to collect all the information associated with a Cauldron project.

* **Parameters**:
  * Cauldron project **[model]**
* **Creation conditions**:
  * When a user requests to download the data associated with a project
* **Actions at running**:
  * Make calls to the Elasticsearch API to collect the data associated with the selected project
  * Place the collected data somewhere where the user can download it later
* **Actions at finish**:
  * Enable data download for the user who requested them
